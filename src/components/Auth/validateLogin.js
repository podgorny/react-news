export default function validateLogin(values) {
  let errs = {};

  // email errors
  if (!values.email) {
    errs.email = "Email required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errs.email = "Invaid email address";
  }
  // password errors
  if (!values.password) {
    errs.password = "Password required";
  } else if (values.password.length < 6) {
    errs.password = "Password must be at least 6 characters";
  }

  return errs;
}
