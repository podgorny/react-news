import React, { useContext, useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import distanceInWordsToNow from "date-fns/formatDistanceToNow";
import { FirebaseContext } from "../../firebase";

function LinkItem({ link, index, showCount, history }) {
  const { firebase, user } = useContext(FirebaseContext);
  const [isVoted, setIsVoted] = useState(false);

  useEffect(() => {
    if (user) {
      firebase.db
        .collection("links")
        .doc(link.id)
        .get()
        .then(doc => {
          const votes = doc.data().votes;
          setIsVoted(votes.map(obj => obj.votedBy.id).includes(user.uid));
        });
    }
    return () => setIsVoted(false);
  }, [user]);

  async function handleVote() {
    if (!user) {
      history.push("/login");
    } else {
      const voteRef = firebase.db.collection("links").doc(link.id);
      const doc = await voteRef.get();
      if (doc.exists) {
        const previousVotes = doc.data().votes;
        const vote = { votedBy: { id: user.uid, name: user.displayName } };
        let updatedVotes = [];
        let voteCount = 0;
        if (!isVoted) {
          updatedVotes = [...previousVotes, vote];
          voteCount = updatedVotes.length;
          voteRef.update({ votes: updatedVotes, voteCount });
          setIsVoted(true);
        } else {
          updatedVotes = [
            ...previousVotes.filter(obj => obj.votedBy.id !== user.uid)
          ];
          voteCount = updatedVotes.length;
          voteRef.update({ votes: updatedVotes, voteCount });
          setIsVoted(false);
        }
      }
    }
  }

  function handleDeleteLink() {
    const linkRef = firebase.db.collection("links").doc(link.id);
    linkRef
      .delete()
      .then(() => {
        console.log(`Document with ID ${link.id} deleted`);
      })
      .catch(err => {
        console.error("Error deleting document:", err);
      });
  }

  const getDomain = url =>
    url
      .replace("http://", "")
      .replace("https://", "")
      .split(/[/?#]/)[0];

  const postedByAuthUser = user && user.uid === link.postedBy.id;

  return (
    <section
      className={`link-item link-item-${link.description
        .toLowerCase()
        .replace(/[ ,]+/g, "-")}`}
    >
      <div className="link-item__prefix">
        {showCount && <span className="link-item__count">{index}.</span>}
        <button
          className="link-item__upvote-button icon-button"
          type="button"
          onClick={handleVote}
          title="upvote"
        >
          <span
            role="img"
            aria-label="upvote"
            className={`icon-button__icon ${isVoted ? "grey" : ""}`}
          >
            👍
          </span>
        </button>
      </div>

      <div className="link-item__body">
        <div className="link-item__title link-item-title">
          <a
            className="link-item-title__description"
            href={link.url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {link.description}
          </a>
          <span className="link-item-title__domain">
            ({getDomain(link.url)})
          </span>
        </div>
        <div className="link-item__subtext-about">
          {link.voteCount} votes by {link.postedBy.name}{" "}
          {distanceInWordsToNow(link.created)}
          <Link to={`/link/${link.id}`} className="link-item__comment">
            {link.comments.length > 0
              ? `${link.comments.length} comments`
              : "discuss"}
          </Link>
        </div>
      </div>
      {postedByAuthUser && (
        <button
          onClick={handleDeleteLink}
          type="button"
          title="delete"
          className="link-item__delete-button icon-button"
        >
          <span role="img" aria-label="delete" className="icon-button__icon">
            🗑️
          </span>
        </button>
      )}
    </section>
  );
}

export default withRouter(LinkItem);
