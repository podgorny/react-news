function validateCreateLink(values) {
  let errs = {};

  if (!values.description) {
    errs.description = "Description required";
  } else if (values.description.length < 10) {
    errs.description = "Description must be at least 10 characters";
  } else if (values.description.length > 120) {
    errs.description = "Description must be no longer than 120 characters";
  }

  if (!values.url) {
    errs.url = "URL required";
  } else if (!/^(ftp|http|https):\/\/[^ "]+$/.test(values.url)) {
    errs.url = "URL must be valid";
  }

  return errs;
}

export default validateCreateLink;
