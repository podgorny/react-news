import React, { useContext } from "react";
import useFormValidation from "../Auth/useFormValidation";
import validateCreateLink from "./validateCreateLink";
import { FirebaseContext } from "../../firebase";

const INITIAL_STATE = {
  description: "",
  url: ""
};

function CreateLink(props) {
  const { firebase, user } = useContext(FirebaseContext);

  const { handleSubmit, handleChange, values, errors } = useFormValidation(
    INITIAL_STATE,
    validateCreateLink,
    handleCreateLink
  );

  function handleCreateLink() {
    if (!user) {
      props.history.push("/login");
    } else {
      const { url, description } = values;
      const newLink = {
        url,
        description,
        postedBy: {
          id: user.uid,
          name: user.displayName
        },
        voteCount: 0,
        votes: [],
        comments: [],
        created: Date.now()
      };
      firebase.db.collection("links").add(newLink);
      props.history.push("/");
    }
  }

  return (
    <form onSubmit={handleSubmit} className="create-link-form">
      <input
        name="description"
        placeholder="a description to your link"
        autoComplete="off"
        type="text"
        value={values.description}
        onChange={handleChange}
        className={`create-link-form__description create-link-form__input input ${
          errors.description ? "error-input" : ""
        }`}
      />
      {errors.description && <p className="error-text">{errors.description}</p>}
      <input
        name="url"
        placeholder="the URL for the link"
        autoComplete="off"
        type="url"
        value={values.url}
        onChange={handleChange}
        className={`create-link-form__url create-link-form__input input ${
          errors.url ? "error-input" : ""
        }`}
      />
      {errors.url && <p className="error-text">{errors.url}</p>}
      <button type="submit" className="create-link-form__submit-button button">
        submit
      </button>
    </form>
  );
}

export default CreateLink;
