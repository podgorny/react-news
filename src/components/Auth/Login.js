import React, { useState } from "react";
import useFormValidation from "./useFormValidation";
import validateLogin from "./validateLogin";
import { firebase } from "../../firebase";
import { Link } from "react-router-dom";

const INITIAL_STATE = {
  name: "",
  email: "",
  password: ""
};

function Login(props) {
  const {
    handleChange,
    handleSubmit,
    handleBlur,
    errors,
    isSubmitting,
    values
  } = useFormValidation(INITIAL_STATE, validateLogin, authenticateUser);
  const [login, setLogin] = useState(true);
  const [firebaseError, setFirebaseError] = useState(null);

  async function authenticateUser() {
    const { name, email, password } = values;
    try {
      login
        ? await firebase.login(email, password)
        : await firebase.register(name, email, password);
      props.history.push("/");
    } catch (err) {
      console.log("Authentication Error", err);
      setFirebaseError(err.message);
    }
  }

  return (
    <div className="login">
      <h2 className="login__title title">
        {login ? "Login" : "Create Account"}
      </h2>
      <form className="login__form login-form">
        {!login && (
          <input
            onChange={handleChange}
            name="name"
            value={values.name}
            className="login-form__name login-form__field input"
            type="text"
            placeholder="your name"
            autoComplete="off"
            required
          />
        )}
        <input
          onBlur={handleBlur}
          onChange={handleChange}
          name="email"
          value={values.email}
          className={`login-form__email login-form__field input ${
            errors.email ? "error-input" : ""
          }`}
          type="email"
          placeholder="your email"
          autoComplete="off"
          required
        />
        {errors.email && <p className="error-text">{errors.email}</p>}
        <input
          onBlur={handleBlur}
          onChange={handleChange}
          name="password"
          values={values.password}
          className={`login-form__password login-form__field input ${
            errors.password ? "error-input" : ""
          }`}
          type="password"
          placeholder="here goes your password"
          required
        />
        {errors.password && <p className="error-text">{errors.password}</p>}
        {firebaseError && <p className="error-text">{firebaseError}</p>}

        <div className="submit-wrapper">
          <button
            onClick={handleSubmit}
            className="login-form__submit-button button"
            type="submit"
            disabled={isSubmitting}
          >
            submit
          </button>
          <div className="login-form__forgot-password">
            <Link className="forgot-password__link" to="/forgot">
              forgot password?
            </Link>
          </div>
        </div>
        <button
          className="login-form__button_change-login button_transparent"
          type="button"
          onClick={() => setLogin(prev => !prev)}
        >
          {login ? "need to create an account?" : "already have an account?"}
        </button>
      </form>
    </div>
  );
}

export default Login;
