import React, { Suspense, lazy } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Header from "./Header";
import useAuth from "./Auth/useAuth";
import { firebase, FirebaseContext } from "../firebase/";

const Login = lazy(() => import("./Auth/Login"));
const LinksList = lazy(() => import("./Link/LinksList"));
const ForgotPassword = lazy(() => import("./Auth/ForgotPassword"));
const CreateLink = lazy(() => import("./Link/CreateLink"));
const SearchLinks = lazy(() => import("./Link/SearchLinks"));
const LinkDetail = lazy(() => import("./Link/LinkDetail"));

function App() {
  const user = useAuth();

  return (
    <Router>
      <FirebaseContext.Provider value={{ user, firebase }}>
        <div className="app-container">
          <Header />
          <Suspense
            fallback={<div style={{ alignSelf: "center" }}>Loading...</div>}
          >
            <main className="route-container">
              <Switch>
                <Route exact path="/" render={() => <Redirect to="/new/1" />} />
                <Route path="/login" component={Login} />
                <Route path="/forgot" component={ForgotPassword} />
                <Route path="/new/:page" component={LinksList} />
                <Route path="/top" component={LinksList} />
                <Route path="/create" component={CreateLink} />
                <Route path="/search" component={SearchLinks} />
                <Route path="/link/:linkId" component={LinkDetail} />
              </Switch>
            </main>
          </Suspense>
        </div>
      </FirebaseContext.Provider>
    </Router>
  );
}

export default App;
