import React, { useContext } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { FirebaseContext } from "../firebase";

function Header() {
  const { user, firebase } = useContext(FirebaseContext);

  return (
    <header className="header">
      <nav className="header__nav nav">
        <NavLink to="/" className="header-title">
          <figure>
            <img
              src="/logo.png"
              alt="News React logo"
              className="header__logo logo"
            />
            <figcaption>React News</figcaption>
          </figure>
        </NavLink>
        <ul className="nav__list nav-menu">
          <li className="nav-menu__item">
            <NavLink to="/" className="nav-menu__link">
              new
            </NavLink>
          </li>
          <li className="nav-menu__item">
            <NavLink to="/top" className="nav-menu__link">
              top
            </NavLink>
          </li>
          <li className="nav-menu__item">
            <NavLink to="/search" className="nav-menu__link">
              search
            </NavLink>
          </li>
          {user && (
            <li className="nav-menu__item">
              <NavLink to="/create" className="nav-menu__link">
                submit
              </NavLink>
            </li>
          )}
        </ul>
      </nav>
      <ul className="nav__list nav-user">
        <li className="nav-user__item">
          {user ? (
            <div className="header-username">{user.displayName}</div>
          ) : (
            <NavLink to="/login" className="nav-user__login-link">
              login
            </NavLink>
          )}
        </li>
        {user && (
          <li className="nav-user__item">
            <div
              className="nav-user__logout-button"
              onClick={() => firebase.logout()}
            >
              logout
            </div>
          </li>
        )}
      </ul>
    </header>
  );
}

export default withRouter(Header);
