import React, { useState, useEffect, useContext } from "react";
import { FirebaseContext } from "../../firebase";
import LinkItem from "./LinkItem";

function SearchLinks() {
  const { firebase } = useContext(FirebaseContext);
  const [filter, setFilter] = useState("");
  const [links, setLinks] = useState([]);
  const [filteredLinks, setfilteredLinks] = useState([]);

  useEffect(() => {
    getInitialLinks();
  }, []);

  function getInitialLinks() {
    firebase.db
      .collection("links")
      .get()
      .then(snap => {
        const links = snap.docs.map(doc => {
          return { id: doc.id, ...doc.data() };
        });
        setLinks(links);
      });
  }

  function handleSearch(e) {
    e.preventDefault();
    const query = filter.toLowerCase();
    const matchedLinks = links.filter(link => {
      return (
        link.description.toLowerCase().includes(query) ||
        link.url.toLowerCase().includes(query) ||
        link.postedBy.name.toLowerCase().includes(query)
      );
    });
    setfilteredLinks(matchedLinks);
  }

  return (
    <>
      <form onSubmit={handleSearch} className="search-form">
        <label
          className="input-label search-input__label"
          htmlFor="search-field"
        >
          Search
        </label>
        <input
          id="search-field"
          type="search"
          onChange={event => setFilter(event.target.value)}
          className="search-form__search-field search-input input"
          autoComplete="off"
        />
        <button
          className="icon-button search-form__search-button"
          title="search"
        >
          <span
            role="img"
            aria-label="search button"
            className="icon-button__icon "
          >
            🔎
          </span>
        </button>
      </form>
      {filteredLinks.map((link, index) => (
        <LinkItem key={link.id} showCount={false} link={link} index={index} />
      ))}
    </>
  );
}

export default SearchLinks;
