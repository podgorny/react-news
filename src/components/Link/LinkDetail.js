import React, { useEffect, useContext, useState } from "react";
import { FirebaseContext } from "../../firebase";
import LinkItem from "./LinkItem";
import distanceInWordsToNow from "date-fns/formatDistanceToNow";

function LinkDetail(props) {
  const { firebase, user } = useContext(FirebaseContext);
  const [link, setLink] = useState(null);
  const [commentText, setCommentText] = useState("");
  const linkId = props.match.params.linkId;
  const linkRef = firebase.db.collection("links").doc(linkId);

  useEffect(() => {
    getLink();
  }, []);

  function getLink() {
    linkRef.get().then(doc => {
      setLink({ ...doc.data(), id: doc.id });
    });
  }

  function handleAddComment() {
    if (!user) {
      props.history.push("/login");
    } else {
      linkRef.get().then(doc => {
        if (doc.exists) {
          const prevComments = doc.data().comments;
          const comment = {
            postedBy: { id: user.uid, name: user.displayName },
            created: Date.now(),
            text: commentText
          };
          const updatedComments = [...prevComments, comment];
          linkRef.update({ comments: updatedComments });
          setLink(prev => ({
            ...prev,
            comments: updatedComments
          }));
          setCommentText("");
        }
      });
    }
  }

  return !link ? (
    <div>Loading...</div>
  ) : (
    <div>
      <LinkItem showCount={false} link={link} />
      <div className="add-comment">
        <textarea
          className="add-comment__field"
          rows="6"
          cols="60"
          onChange={e => setCommentText(e.target.value)}
          value={commentText}
        />

        <button
          className="button add-comment__button"
          onClick={handleAddComment}
        >
          add comment
        </button>
      </div>

      {link.comments.map((comment, inx) => (
        <div key={inx} className="comment">
          <p className="comment__details">
            <span className="comment__author">{comment.postedBy.name}</span>
            <span className="comment__posted-time">
              {distanceInWordsToNow(comment.created)}
            </span>
          </p>
          <p className="comment-text">{comment.text}</p>
        </div>
      ))}
    </div>
  );
}

export default LinkDetail;
