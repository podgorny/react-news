import React, { useState, useContext } from "react";
import { FirebaseContext } from "../../firebase";

function ForgotPassword() {
  const { firebase } = useContext(FirebaseContext);
  const [resetPasswordEmail, setResetPasswordEmail] = useState("");
  const [isPasswordReset, setIsPasswordReset] = useState(false);
  const [passwordResetError, setPasswordResetError] = useState(null);

  async function handleResetPassword(e) {
    e.preventDefault();
    try {
      await firebase.resetPassword(resetPasswordEmail);
      setIsPasswordReset(true);
      setPasswordResetError(null);
    } catch (err) {
      console.error("Error sending email", err);
      setPasswordResetError(err.message);
      setIsPasswordReset(false);
    }
  }

  return (
    <form className="forgot-password">
      <input
        type="email"
        className={
          passwordResetError
            ? "forgot-password__field input error-input"
            : "forgot-password__field input"
        }
        placeholder="provide your account email"
        onChange={e => setResetPasswordEmail(e.target.value)}
      />

      <button
        type="submit"
        onClick={handleResetPassword}
        className="forgot-password__reset-button button"
      >
        reset
      </button>

      {isPasswordReset && (
        <p className="success-text">✔ check email to reset password</p>
      )}
      {passwordResetError && <p className="error-text">{passwordResetError}</p>}
    </form>
  );
}

export default ForgotPassword;
